package com.infuturewetrust.tornado;

import com.squareup.okhttp.OkHttpClient;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class HttpManager {
    private static class InstanceHolder {
        private static final OkHttpClient instance = new OkHttpClient();
    }

    private static final String USER_AGENT = "OkHttp";

    public static HttpResponse get(URL url) throws IOException {
        return get(url, null);
    }

    public static HttpResponse get(URL url, HttpEntity entity) throws IOException {
        InputStream in = null;
        HttpResponse.Builder builder = new HttpResponse.Builder();
        HttpURLConnection connection = InstanceHolder.instance.open(url);

        if (entity != null && entity.hasCustomHeaders()) {
            setCustomHeaders(connection, entity.getHeaders());
        }

        try {
            in = connection.getInputStream();
            builder.setBody(HttpEntity.readResponseBody(in));
        } finally {
            if (in != null) in.close();
        }

        builder.setResponseCode(connection.getResponseCode());
        builder.setResponseMessage(connection.getResponseMessage());
        builder.setResponseHeaders(connection.getHeaderFields());

        return builder.build();
    }

    public static HttpResponse post(URL url, HttpEntity entity) throws IOException {
        InputStream in = null;
        OutputStream out = null;
        PrintWriter writer = null;
        HttpResponse.Builder builder = new HttpResponse.Builder();
        HttpURLConnection connection = InstanceHolder.instance.open(url);

        if (entity == null)
            throw new NullPointerException("Entity cannot be null");

        if (entity.hasCustomHeaders())
            setCustomHeaders(connection, entity.getHeaders());

        connection.setRequestMethod("POST");
        connection.setInstanceFollowRedirects(entity.followRedirect());
        connection.setRequestProperty("User-Agent", USER_AGENT);
        if (entity.isMultipart()) {
            connection.setRequestProperty("Accept", "*/*");
            connection.setRequestProperty("Content-Type",
                    "multipart/form-data; boundary=" + HttpEntity.BOUNDARY);
        }

        try {
            out = connection.getOutputStream();
            writer = new PrintWriter(new OutputStreamWriter(out, "utf-8"), true);
            entity.writeData(out, writer);
            in = connection.getInputStream();
            builder.setBody(HttpEntity.readResponseBody(in));
        } finally {
            if (in != null) in.close();
            if (out != null) out.close();
            if (writer != null) writer.close();
        }

        builder.setResponseCode(connection.getResponseCode());
        builder.setResponseMessage(connection.getResponseMessage());
        builder.setResponseHeaders(connection.getHeaderFields());

        return builder.build();
    }

    public static HttpResponse delete(URL url, HttpEntity entity) throws IOException {
        return customRequest(url, entity, "DELETE");
    }

    public static HttpResponse put(URL url, HttpEntity entity) throws IOException {
        return customRequest(url, entity, "PUT");
    }

    /**
     * Helper method to perform HTTP request other than GET
     * @param url URL of the resource
     * @param entity request body
     * @param method of the request
     * @return HttpResponse
     * @throws IOException
     */
    private static HttpResponse customRequest(URL url, HttpEntity entity, String method) throws IOException {
        HttpURLConnection connection = InstanceHolder.instance.open(url);
        HttpResponse.Builder builder = new HttpResponse.Builder();
        InputStream in = null;
        OutputStream out = null;
        PrintWriter writer = null;

        if (entity == null)
            throw new NullPointerException("Entity cannot be null");

        if (entity.hasCustomHeaders())
            setCustomHeaders(connection, entity.getHeaders());

        connection.setRequestMethod(method);
        connection.setInstanceFollowRedirects(entity.followRedirect());
        connection.setRequestProperty("User-Agent", USER_AGENT);
        try {
            out = connection.getOutputStream();
            writer = new PrintWriter(new OutputStreamWriter(out, "utf-8"), true);
            entity.writeData(out, writer);
            in = connection.getInputStream();
            builder.setBody(HttpEntity.readResponseBody(in));
        } finally {
            if (in != null) in.close();
            if (out != null) out.close();
            if (writer != null) writer.close();
        }

        builder.setResponseCode(connection.getResponseCode());
        builder.setResponseMessage(connection.getResponseMessage());
        builder.setResponseHeaders(connection.getHeaderFields());

        return builder.build();
    }

    private static void setCustomHeaders(HttpURLConnection connection, Map<String, String> headers) {
        for (Map.Entry<String, String> header: headers.entrySet()) {
            connection.addRequestProperty(header.getKey(), header.getValue());
        }
    }

}
