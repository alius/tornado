package com.infuturewetrust.tornado;

import java.util.List;
import java.util.Map;

public class HttpResponse {
    private byte[] mBody;
    private int mResponseCode;
    private String mResponseMessage;
    private Map<String, List<String>> mResponseHeaders;

    private HttpResponse(Builder builder) {
        mBody = builder.body;
        mResponseCode = builder.responseCode;
        mResponseMessage = builder.responseMessage;
        mResponseHeaders = builder.responseHeaders;
    }

    public byte[] getBody() {
        return mBody;
    }

    public int getResponseCode() {
        return mResponseCode;
    }

    public String getResponseMessage() {
        return mResponseMessage;
    }

    public Map<String, List<String>> getResponseHeaders() {
        return mResponseHeaders;
    }

    public static class Builder {
        private byte[] body;
        private int responseCode;
        private String responseMessage;
        private Map<String, List<String>> responseHeaders;

        public Builder() {
        }

        public void setBody(byte[] body) {
            this.body = body;
        }

        public void setResponseCode(int responseCode) {
            this.responseCode = responseCode;
        }

        public void setResponseMessage(String message) {
            this.responseMessage = message;
        }

        public void setResponseHeaders(Map<String, List<String>> responseHeaders) {
            this.responseHeaders = responseHeaders;
        }

        public HttpResponse build() {
            return new HttpResponse(this);
        }
    }
}
