package com.infuturewetrust.tornado;

import java.io.*;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This utility class provides an abstraction layer for sending multipart HTTP
 * POST requests to a web server.
 *
 * @author Future Industries
 */
public class HttpEntity {
    public static final String BOUNDARY;
    private static final String BOUNDARY_START = "--";
    private static final String CRLF = "\r\n";

    private boolean mFollowRedirect;
    private Map<String, String> mHeaders;
    private Map<String, String> mFormFields;
    private Map<String, FileContainer> mFormFiles;

    static {
        BOUNDARY = "---" + System.currentTimeMillis();
    }

    public HttpEntity() {
        mHeaders = new LinkedHashMap<String, String>();
        mFormFields = new LinkedHashMap<String, String>();
        mFormFiles = new LinkedHashMap<String, FileContainer>();
    }

    public void addFormField(String name, String value) {
        mFormFields.put(name, value);
    }

    public void addFilePart(String fieldName, String fileName, byte[] file) {
        mFormFiles.put(fieldName, new FileContainer(fileName, file));
    }

    public void addHeader(String name, String value) {
        mHeaders.put(name, value);
    }

    /**
     * Get a Map<String, String> with headers
     * @return headers Map with custom headers
     */
    public Map<String, String> getHeaders() {
        return mHeaders;
    }

    /**
     * Checks if there are any custom headers set
     * @return result true if custom headers present
     */
    public boolean hasCustomHeaders() {
        return mHeaders.size() > 0;
    }

    /**
     * Adds a form field to the request
     *
     * @param writer PrintWriter
     * @param name  field name
     * @param value field value
     */
    private void addFormFieldMultipart(PrintWriter writer, String name, String value) {
        final String contentDisp = "Content-Disposition: form-data; name=\"" + name + "\"";
        writer.print(BOUNDARY_START + BOUNDARY + CRLF);
        writer.print(contentDisp + CRLF);
        writer.print(CRLF);
        writer.print(value + CRLF);
    }

    /**
     * Adds a upload file section to the request
     *
     * @param name     attribute in <input type="file" name="..." />
     * @param fileName of the uploaded file
     * @param file     bytearray with file contents
     */
    private void addFilePartMultipart(PrintWriter writer, OutputStream out, String name, String fileName, byte[] file) throws IOException {
        final String contentDisp = "Content-Disposition: form-data; name=\"" +
                name + "\"; filename=\"" + fileName + "\"";
        final String contentType = "Content-Type: " + URLConnection.guessContentTypeFromName(fileName);

        writer.print(BOUNDARY_START + BOUNDARY + CRLF);
        writer.print(contentDisp + CRLF);
        writer.print(contentType + CRLF);
        writer.print(CRLF);
        writer.flush();
        out.write(file, 0, file.length);
        out.flush();
    }

    /**
     * Writes data to the channel
     * @param out OutputStream
     * @param writer PrintWriter
     * @throws IOException
     */
    public void writeData(OutputStream out, PrintWriter writer) throws IOException {
        if (isMultipart()) {
            for (Map.Entry<String, String> entry : mFormFields.entrySet()) {
                addFormFieldMultipart(writer, entry.getKey(), entry.getValue());
            }
            writer.flush();
            for (Map.Entry<String, FileContainer> entry : mFormFiles.entrySet()) {
                addFilePartMultipart(writer, out, entry.getKey(), entry.getValue().fileName,
                        entry.getValue().fileContents);
            }

            writer.print(CRLF);
            writer.print(BOUNDARY_START + BOUNDARY + BOUNDARY_START + CRLF);
            writer.flush();
        } else {
            StringBuilder builder = new StringBuilder();
            for (Map.Entry<String, String> entry : mFormFields.entrySet()) {
                String key = URLEncoder.encode(entry.getKey(), "UTF-8");
                String value = URLEncoder.encode(entry.getValue(), "UTF-8");
                builder.append(key).append("=").append(value).append("&");
            }

            if (builder.length() > 0)
                builder.deleteCharAt(builder.length() - 1);

            writer.print(builder.toString());
            writer.flush();
        }
    }

    public boolean isMultipart() {
        return ! mFormFiles.isEmpty();
    }

    public boolean followRedirect() {
        return mFollowRedirect;
    }

    public void setFollowRedirect(boolean follow) {
        mFollowRedirect = follow;
    }

    private class FileContainer {
        public String fileName;
        public byte[] fileContents;

        public FileContainer(String name, byte[] file) {
            fileName = name;
            fileContents = file;
        }

        @Override
        public int hashCode() {
            return fileName.hashCode();
        }

        @Override
        public String toString() {
            return fileName;
        }
    }

    /**
     * Converts the InputStream to byte array
     * @param in InputStream
     * @return byte array
     * @throws IOException
     */
    public static byte[] readResponseBody(InputStream in) throws IOException{
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int read;
        byte[] data = new byte[2048];
        while ((read = in.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, read);
        }
        buffer.flush();
        return buffer.toByteArray();
    }
}